'use strict';

/*
Copyright (c) 2022, Butterfly
All rights reserved.
Version: 1.0.4
Author: Hatim Hoho
date: 2022-09-16
*/

const useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
const isSmallScreen = window.matchMedia('(max-width: 1023.5px)').matches;

// full features of tinymce version 6 plugins
    var mcePlugins = [
        'preview', 'importcss', 'searchreplace', 'autolink', 
        'directionality',
        'code', 'visualblocks', 'visualchars',
         'fullscreen', 'image', 'link', 'media', 
         'template', 'codesample', 'table', 'charmap',
          'pagebreak', 'nonbreaking', 'anchor', 'insertdatetime',
           'advlist', 'lists', 'wordcount', 'help', 
           'charmap', 'quickbars', 'emoticons',
           'wagtailimage', 'wagtaillink', 'wagtaildoclink', 'wagtailembeds',
    ],
    mceTools = ['wagtailimage media undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview print | insertfile template link anchor codesample | ltr rtl'], // TinyMCE tools
    mceExternalPlugins = {};

function registerMCEPlugin(name, path, language) {
    // return;
    if (path) {
        mceExternalPlugins[name] = path;
        if (language) {
            tinymce.PluginManager.requireLangPack(name, language);
        }
    } else {
        mcePlugins.push(name);
    }
}

function registerMCETool(name) {
    mceTools.push(name);
}

function makeTinyMCEEditable(id, kwargs) {

    kwargs = kwargs || {};
    $.extend(kwargs, {
        selector: '#' + id.toString(),
        plugins: mcePlugins,
        editimage_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: mceTools,
        toolbar_sticky: false,
        toolbar_sticky_offset: isSmallScreen ? 102 : 108,
        language: 'en',
        // image_advtab: true,
        importcss_append: true,
        template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
        height: 600,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | quicklink h1 h2 h3 blockquote quickimage quicktable',
        noneditable_class: 'mceNonEditable',
        toolbar_mode: 'sliding',
        contextmenu: 'link image table',
        // skin: useDarkMode ? 'oxide-dark' : 'oxide',
        skin: 'oxide',
        // content_css: useDarkMode ? 'dark' : 'default',
        content_css: 'default',
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }',
        external_plugins: mceExternalPlugins,
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

    setTimeout(function () {
        tinymce.init(kwargs);
    }, 1);
}
