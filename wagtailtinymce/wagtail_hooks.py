# Copyright (c) 2016, Isotoma Limited
# All rights reserved.

# Upgraded to TinyMCE 6.2
# Version: 1.0.4
# Author: Hatim Hoho
# date: 2022-09-16

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the Isotoma Limited nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL ISOTOMA LIMITED BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import json

from django import __version__ as DJANGO_VERSION
from django.templatetags.static import static
from django.utils import translation
from django.utils.html import escape
from django.utils.html import format_html
from django.utils.html import format_html_join
from django.utils.safestring import mark_safe

from wagtail import __version__ as WAGTAIL_VERSION
from wagtail.core.whitelist import attribute_rule
from wagtail.admin.rich_text.converters.editor_html import WhitelistRule

if DJANGO_VERSION >= '2.0':
    from django.urls import reverse
else:
    from django.core.urlresolvers import reverse

if WAGTAIL_VERSION >= '2.0':
    from wagtail.admin.templatetags.wagtailadmin_tags import hook_output
    from wagtail.core import hooks
else:
    from wagtail.wagtailadmin.templatetags.wagtailadmin_tags import hook_output
    from wagtail.wagtailcore import hooks


def to_js_primitive(string):
    return mark_safe(json.dumps(escape(string)))


@hooks.register('insert_editor_css')
def insert_editor_css():
    css_files = [
        'wagtailtinymce/css/icons.css'
    ]
    css_includes = format_html_join(
        '\n',
        '<link rel="stylesheet" href="{0}">',
        ((static(filename),) for filename in css_files),
    )
    return css_includes + hook_output('insert_tinymce_css')


def _format_js_includes(js_files):
    return format_html_join(
        '\n',
        '<script src="{0}"></script>',
        ((static(filename),) for filename in js_files)
    )


@hooks.register('insert_editor_js')
def insert_editor_js():
    preload = format_html(
        '<script>'
        '(function() {{'
        '    "use strict";'
        '    window.tinymce = window.tinymce || {{}};'
        '    window.tinymce.base = window.tinymce.baseURL = {};'
        '    window.tinymce.suffix = "";'
        '}}());'
        '</script>',
        to_js_primitive(static('wagtailtinymce/js/vendor/tinymce')),
    )
    js_includes = _format_js_includes([
        'wagtailtinymce/js/vendor/tinymce/jquery.tinymce.min.js',
        'wagtailtinymce/js/vendor/tinymce/tinymce.min.js',
        'wagtailtinymce/js/tinymce-editor.js',
    ])
    return preload + js_includes + hook_output('insert_tinymce_js')


@hooks.register('insert_tinymce_js')
def images_richtexteditor_js():
    preload = format_html(
        """
        <script>
            registerMCEPlugin("wagtailimage", {}, {});
            window.chooserUrls.imageChooserSelectFormat = {};
        </script>
        """,
        to_js_primitive(static('wagtailtinymce/js/tinymce-plugins/wagtailimage.js')),
        to_js_primitive(translation.to_locale(translation.get_language())),
        to_js_primitive(reverse('wagtailimages:chooser_select_format', args=['00000000']))
    )
    js_includes = _format_js_includes([
        'wagtailimages/js/image-chooser-modal.js',
        'wagtailimages/js/image-chooser.js'
    ])
    return preload + js_includes

@hooks.register('insert_tinymce_js')
def embeds_richtexteditor_js():
    preload = format_html(
        """
        <script>
            registerMCEPlugin("wagtailembeds", {}, {});
        </script>
        """,
        to_js_primitive(static('wagtailtinymce/js/tinymce-plugins/wagtailembeds.js')),
        to_js_primitive(translation.to_locale(translation.get_language())),
    )
    js_includes = _format_js_includes([
        'wagtailembeds/js/embed-chooser-modal.js',
    ])
    return preload + js_includes


@hooks.register('insert_tinymce_js')
def links_richtexteditor_js():
    preload = format_html(
        """
        <script>
            registerMCEPlugin("wagtaillink", {}, {});
        </script>
        """,
        to_js_primitive(static('wagtailtinymce/js/tinymce-plugins/wagtaillink.js')),
        to_js_primitive(translation.to_locale(translation.get_language())),
    )
    js_includes = _format_js_includes([
        'wagtailadmin/js/page-chooser.js',
        'wagtailadmin/js/page-chooser-modal.js',
    ])
    return preload + js_includes


@hooks.register('insert_tinymce_js')
def docs_richtexteditor_js():
    preload = format_html(
        """
        <script>
            registerMCEPlugin("wagtaildoclink", {}, {});
        </script>
        """,
        to_js_primitive(static('wagtailtinymce/js/tinymce-plugins/wagtaildoclink.js')),
        to_js_primitive(translation.to_locale(translation.get_language())),
    )
    js_includes = _format_js_includes([
        'wagtaildocs/js/document-chooser.js',
        'wagtaildocs/js/document-chooser-modal.js',
    ])
    return preload + js_includes

@hooks.register('register_rich_text_features')
def span_feature(features):
    # register a feature 'span' which whitelists the <span> element
    features.register_converter_rule('editorhtml', 'span', [
        WhitelistRule('span', attribute_rule({'class': True, 'data-label': True, 'style': True})),
    ])
    # add 'span' to the default feature set
    features.default_features.append('span')

    features.register_converter_rule('editorhtml', 'p', [
        WhitelistRule('p', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'span' to the default feature set
    features.default_features.append('p')


    headings_elements = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'code']
    for element in headings_elements:
        features.register_converter_rule('editorhtml', element, [
            WhitelistRule(element, attribute_rule({'class': True, 'style': True, 'id': True})),
        ])
        if element not in features.default_features:
            features.default_features.append(element)

    features.register_converter_rule('editorhtml', 'table', [
        WhitelistRule('table', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'span' to the default feature set
    features.default_features.append('table')

    features.register_converter_rule('editorhtml', 'tbody', [
        WhitelistRule('tbody', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'span' to the default feature set
    features.default_features.append('tbody')

    features.register_converter_rule('editorhtml', 'td', [
        WhitelistRule('td', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'span' to the default feature set
    features.default_features.append('td')

    features.register_converter_rule('editorhtml', 'ul', [
        WhitelistRule('ul', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'ul' to the default feature set
    features.default_features.append('ul')

    features.register_converter_rule('editorhtml', 'li', [
        WhitelistRule('li', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'li' to the default feature set
    features.default_features.append('li')
    
    features.register_converter_rule('editorhtml', 'tr', [
        WhitelistRule('tr', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'tr' to the default feature set
    features.default_features.append('tr')

    features.register_converter_rule('editorhtml', 'ol', [
        WhitelistRule('ol', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'ol' to the default feature set
    features.default_features.append('ol')
    
    features.register_converter_rule('editorhtml', 's', [
        WhitelistRule('s', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'ol' to the default feature set
    features.default_features.append('s')
    
    features.register_converter_rule('editorhtml', 'em', [
        WhitelistRule('em', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'ol' to the default feature set
    features.default_features.append('em')
    
    features.register_converter_rule('editorhtml', 'br', [
        WhitelistRule('br', attribute_rule({'class': True, 'style': True})),
    ])
    # add 'ol' to the default feature set
    features.default_features.append('br')
