/*
Copyright (c) 2022, Butterfly
All rights reserved.

Author: Hatim Hoho
date: 2022-09-14
*/

(function() {
    'use strict';

    function createLink(pageData, currentText)
    {
        var a, text;

        // Create link
        a = document.createElement('a');
        a.setAttribute('href', pageData.url);
        if (pageData.id) {
            a.setAttribute('data-id', pageData.id);
            a.setAttribute('data-linktype', 'document');
            text = currentText || pageData.title;
        }
        else {
            text = pageData.title;
        }
        a.appendChild(document.createTextNode(text));

        return a;
    }

    (function($) {
        tinymce.PluginManager.add('wagtaildoclink', function (editor) {

            function showDialog() {
                var url, urlParams, mceSelection, $currentNode, $targetNode, currentText, insertElement;

                currentText = '';
                url = window.chooserUrls.documentChooser;
                urlParams = {};

                mceSelection = editor.selection;
                $currentNode = $(mceSelection.getEnd());
                // target selected link (if any)
                $targetNode = $currentNode.closest('a[href]');

                if ($targetNode.length) {
                    currentText = $targetNode.text();
                    if( $targetNode.children().length == 0 )
                    {
                        // select and replace text-only target
                        insertElement = function(elem) {
                            mceSelection.select($targetNode.get(0));
                            mceSelection.setNode(elem);
                        };
                    }
                    else {
                        // replace attributes of complex target
                        insertElement = function(elem) {
                            mceSelection.select($targetNode.get(0));
                            var $elem = $(elem);
                            $targetNode.attr('href', $elem.attr('href'));
                            if ($elem.data('linktype')) {
                                $targetNode.data($elem.data());
                            }
                            else {
                                $targetNode.removeData('linktype');
                                $targetNode.removeAttr('data-linktype');
                            }
                        };
                    }
                }
                else {
                    if (!mceSelection.isCollapsed()) {
                        currentText = mceSelection.getContent({format: 'text'});
                    }
                    // replace current selection
                    insertElement = function(elem) {
                        mceSelection.setNode(elem);
                    };
                }

                ModalWorkflow({
                    url: url,
                    urlParams: urlParams,
                    onload: DOCUMENT_CHOOSER_MODAL_ONLOAD_HANDLERS,
                    responses: {
                        documentChosen: function(pageData) {
                            editor.undoManager.transact(function() {
                                editor.focus();
                                insertElement(createLink(pageData, currentText));
                            });
                        }
                    }
                });
            }

            editor.ui.registry.addButton('wagtaildoclink', {
                icon: 'doc-full',
                tooltip: 'Insert/edit document',
                onAction: function () {
                    // Open window
                    showDialog();
                  },
                stateSelector: 'a[data-linktype=document]'
            });

            editor.ui.registry.addMenuItem('wagtaildoclink', {
                icon: 'doc-full',
                text: 'Insert/edit document',
                onAction: function () {
                    // Open window
                    showDialog();
                  },
                context: 'insert',
                prependToContext: true
            });

            editor.addCommand('mceWagtailDocument', showDialog);
        });
    })(jQuery);

}).call(this);
